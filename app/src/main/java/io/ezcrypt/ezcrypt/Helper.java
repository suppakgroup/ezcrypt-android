package io.ezcrypt.ezcrypt;

import android.content.Context;
import android.content.res.Resources;

import java.security.KeyPair;

import io.ezcrypt.ezcrypt.encryptor.Encryptor;

/**
 * Created by suppakng on 7/23/2017.
 */

public class Helper {

    private static String mode="";
    private static Helper instance;
    private static String pathE;

    public static Context getContext() {
        return context;
    }

    public static void setContext(Context context) {
        Helper.context = context;
    }

    private static Context context;

    private static String pathD;
    private static Resources res;
    private final static String keyPairString = "rO0ABXNyABVqYXZhLnNlY3VyaXR5LktleVBhaXKXAww60s0SkwIAAkwACnByaXZhdGVLZXl0ABpMamF2YS9zZWN1cml0eS9Qcml2YXRlS2V5O0wACXB1YmxpY0tleXQAGUxqYXZhL3NlY3VyaXR5L1B1YmxpY0tleTt4cHNyABRqYXZhLnNlY3VyaXR5LktleVJlcL35T7OImqVDAgAETAAJYWxnb3JpdGhtdAASTGphdmEvbGFuZy9TdHJpbmc7WwAHZW5jb2RlZHQAAltCTAAGZm9ybWF0cQB+AAVMAAR0eXBldAAbTGphdmEvc2VjdXJpdHkvS2V5UmVwJFR5cGU7eHB0AANSU0F1cgACW0Ks8xf4BghU4AIAAHhwAAACezCCAncCAQAwDQYJKoZIhvcNAQEBBQAEggJhMIICXQIBAAKBgQCaEh7SS6I+poVxMrWMhlW0yMFnqgLGUjN7M2jTeawvkGGqm5HOippLiUBcZvm56VMuPlLuUDhGF897bZ5NEbRds7BRfEQOHBE3o1OS2ZPJKelrMp5UmUEvy6x6JSVo3di2zsjgmqnm7agRl7b9sce0I1anxjz+DNgLKH6REAui+QIDAQABAoGAQseT4Gaz/h/DOxtVoYEgaOmTGtGPvlImykGwL+lO/pbAHmirvDBLTG5DHUns9+3wBd0xqOXQXmE3pzIOKS2BVWfA8AYjiSTeztTDEC/rL+dnvlhgid8zNqRKe3h52/tzTCMGzzfwqrA5MUhrPPdDEWYH/GfTv0JMiztyuXq+cXkCQQDHWiZmJ1ml/4LAvVJJXgghxXRR3h1oxiScY+Qmt9sFgV4/QWCugZDvzsMZN0ulwe3Gm9585q7a7MZHViHt2TGXAkEAxdn7gqinDCPzLmFcNC7oB/s/5L6Jfa/tuCaBBdwqW1lHPIAkv57SiCs7VnZGOIGh3vqtvzcQN6uqUhlW4KlB7wJBAL7ylNFIY9Biu6VOMmFgFj3y2gYI0PaoxCygacrEVKAjWFkSkD7WkDODQtEGbAkrhku0xRmyBE5ieVGfKQSgA6UCQAX5diHU6+5u63n8EKe2vuT2zlAe/NHhzSDhcMX3hwPpJ0CJchKjPiJEg+8xdR9hsryj/zkl9YRo16TRBRGVqtsCQQCizegNncuDzkyqpRVpRIBJ6NogqEs50s3U49ZRkuAUH2U/WKkphSLx/V7o594rqJlVcUvZoOFD3lfzN/Ksz2/fdAAGUEtDUyM4fnIAGWphdmEuc2VjdXJpdHkuS2V5UmVwJFR5cGUAAAAAAAAAABIAAHhyAA5qYXZhLmxhbmcuRW51bQAAAAAAAAAAEgAAeHB0AAdQUklWQVRFc3EAfgAEcQB+AAl1cQB+AAoAAACiMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCaEh7SS6I+poVxMrWMhlW0yMFnqgLGUjN7M2jTeawvkGGqm5HOippLiUBcZvm56VMuPlLuUDhGF897bZ5NEbRds7BRfEQOHBE3o1OS2ZPJKelrMp5UmUEvy6x6JSVo3di2zsjgmqnm7agRl7b9sce0I1anxjz+DNgLKH6REAui+QIDAQABdAAFWC41MDl+cQB+AA10AAZQVUJMSUM=";

    private static KeyPair keyPair;
    private static Serialize serialize = new Serialize();
    public static Encryptor encryptor = new Encryptor();

    public static Helper get() {
        if (instance == null) instance = getSync();
        return instance;
    }

    private static synchronized Helper getSync() {
        if (instance == null) instance = new Helper();
        try{
            setKeyPair((KeyPair)serialize.fromString(keyPairString));
        }catch(Exception e){

        }

        return instance;
    }

    public Resources getResource(){
        return res;
    }

    public void setResource(Resources res){
        this.res = res;
    }

    public static KeyPair getKeyPair() {
        return keyPair;
    }

    private static void setKeyPair(KeyPair keyPair) {
        Helper.keyPair = keyPair;
    }
    public static String getPathD() {return pathD;}

    public static void setPathD(String pathD) {Helper.pathD = pathD;}

    public static String getPathE() {return pathE;}

    public static void setPathE(String pathE) {Helper.pathE = pathE;}

    public static String getMode() {
        return mode;
    }

    public static void setMode(String mode) {
        Helper.mode = mode;
    }
}