package io.ezcrypt.ezcrypt.encoder;

/**
 * Created by suppakng on 6/20/2017.
 */

public interface GoToNextInterface {
    void onGoToNext();
}