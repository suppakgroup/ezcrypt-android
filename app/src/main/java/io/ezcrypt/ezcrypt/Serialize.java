package io.ezcrypt.ezcrypt;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;

/**
 * Created by suppakng on 7/30/2017.
 */

public class Serialize {
    public Serialize(){

    }

    //Example usage

    /*
    public static void main( String [] args )  throws IOException,
            ClassNotFoundException, NoSuchAlgorithmException {

        //Get instance of RSA keygen
        KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
        //Get keys
        KeyPair myPair = kpg.generateKeyPair();

        System.out.println(myPair.getPublic());
        System.out.println(myPair.getPrivate());

        String string = toString(myPair);
        System.out.println(" Encoded serialized version " );
        System.out.println( string );
        //KeyPair some = ( KeyPair ) fromString( string );

        KeyPair some = (KeyPair) fromString("rO0ABXNyABVqYXZhLnNlY3VyaXR5LktleVBhaXKXAww60s0SkwIAAkwACnByaXZhdGVLZXl0ABpMamF2YS9zZWN1cml0eS9Qcml2YXRlS2V5O0wACXB1YmxpY0tleXQAGUxqYXZhL3NlY3VyaXR5L1B1YmxpY0tleTt4cHNyABRqYXZhLnNlY3VyaXR5LktleVJlcL35T7OImqVDAgAETAAJYWxnb3JpdGhtdAASTGphdmEvbGFuZy9TdHJpbmc7WwAHZW5jb2RlZHQAAltCTAAGZm9ybWF0cQB+AAVMAAR0eXBldAAbTGphdmEvc2VjdXJpdHkvS2V5UmVwJFR5cGU7eHB0AANSU0F1cgACW0Ks8xf4BghU4AIAAHhwAAACezCCAncCAQAwDQYJKoZIhvcNAQEBBQAEggJhMIICXQIBAAKBgQCaEh7SS6I+poVxMrWMhlW0yMFnqgLGUjN7M2jTeawvkGGqm5HOippLiUBcZvm56VMuPlLuUDhGF897bZ5NEbRds7BRfEQOHBE3o1OS2ZPJKelrMp5UmUEvy6x6JSVo3di2zsjgmqnm7agRl7b9sce0I1anxjz+DNgLKH6REAui+QIDAQABAoGAQseT4Gaz/h/DOxtVoYEgaOmTGtGPvlImykGwL+lO/pbAHmirvDBLTG5DHUns9+3wBd0xqOXQXmE3pzIOKS2BVWfA8AYjiSTeztTDEC/rL+dnvlhgid8zNqRKe3h52/tzTCMGzzfwqrA5MUhrPPdDEWYH/GfTv0JMiztyuXq+cXkCQQDHWiZmJ1ml/4LAvVJJXgghxXRR3h1oxiScY+Qmt9sFgV4/QWCugZDvzsMZN0ulwe3Gm9585q7a7MZHViHt2TGXAkEAxdn7gqinDCPzLmFcNC7oB/s/5L6Jfa/tuCaBBdwqW1lHPIAkv57SiCs7VnZGOIGh3vqtvzcQN6uqUhlW4KlB7wJBAL7ylNFIY9Biu6VOMmFgFj3y2gYI0PaoxCygacrEVKAjWFkSkD7WkDODQtEGbAkrhku0xRmyBE5ieVGfKQSgA6UCQAX5diHU6+5u63n8EKe2vuT2zlAe/NHhzSDhcMX3hwPpJ0CJchKjPiJEg+8xdR9hsryj/zkl9YRo16TRBRGVqtsCQQCizegNncuDzkyqpRVpRIBJ6NogqEs50s3U49ZRkuAUH2U/WKkphSLx/V7o594rqJlVcUvZoOFD3lfzN/Ksz2/fdAAGUEtDUyM4fnIAGWphdmEuc2VjdXJpdHkuS2V5UmVwJFR5cGUAAAAAAAAAABIAAHhyAA5qYXZhLmxhbmcuRW51bQAAAAAAAAAAEgAAeHB0AAdQUklWQVRFc3EAfgAEcQB+AAl1cQB+AAoAAACiMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCaEh7SS6I+poVxMrWMhlW0yMFnqgLGUjN7M2jTeawvkGGqm5HOippLiUBcZvm56VMuPlLuUDhGF897bZ5NEbRds7BRfEQOHBE3o1OS2ZPJKelrMp5UmUEvy6x6JSVo3di2zsjgmqnm7agRl7b9sce0I1anxjz+DNgLKH6REAui+QIDAQABdAAFWC41MDl+cQB+AA10AAZQVUJMSUM=");
        System.out.println( "\n\nReconstituted object");
        System.out.println(some);
        System.out.println(some.getPublic());
        System.out.println(some.getPrivate());

    }*/

    /** Read the object from Base64 string. */
    public static Object fromString( String s ) throws IOException ,
            ClassNotFoundException {
        byte [] data = Base64.decode( s );
        ObjectInputStream ois = new ObjectInputStream(
                new ByteArrayInputStream(  data ) );
        Object o  = ois.readObject();
        ois.close();
        return o;
    }

    /** Write the object to a Base64 string. */
   /* private static String toString( Serializable o ) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream( baos );
        oos.writeObject( o );
        oos.close();
        return Base64.getEncoder().encodeToString(baos.toByteArray());
    }*/
}


