package io.ezcrypt.ezcrypt;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import io.ezcrypt.ezcrypt.encryptor.Encryptor;

import io.ezcrypt.ezcrypt.encoder.Encoder;
import io.ezcrypt.ezcrypt.encoder.Jstego;

import static io.ezcrypt.ezcrypt.R.id.container;



public class MainActivity extends AppCompatActivity {
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    private TextView mTextMessage;
    private FragmentTransaction fragmentTransaction;
    public ImageButton imagePreview;
    private String inputPath = "";
    private String outputPath = "";
    private String inputMessage = "";
    private File inputFile;
    private Encoder encoder = Encoder.getInstance();


    //testing
    private Encryptor encryptor = Encryptor.getInstance();
    Helper helper = Helper.get();
    Notification notify = Notification.get();

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.encrypt_home:
                    ((ViewPager) findViewById(container)).setCurrentItem(0);
                    return true;
                case R.id.decrypt_screen:
                    ((ViewPager) findViewById(container)).setCurrentItem(1);
                    return true;
                case R.id.about_screen:
                    ((ViewPager) findViewById(container)).setCurrentItem(2);
                    return true;
            }
            return false;
        }

    };

    //Notification
    NotificationCompat.Builder notification;
    private static final int uniqueID = 12345;

    //Notification

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Helper.setContext(this);
        //--------------------Initialization --------------------------------------

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        notification = new NotificationCompat.Builder(this);
        notification.setAutoCancel(true);

        helper.setResource(getResources());
        notify.setSnackBar(findViewById(android.R.id.content),"",0);

        //VM ignores the file URI exposure used to fix file path issue with SDK 24 >
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        ActivityCompat.requestPermissions(MainActivity.this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},1);

        if (shouldAskPermissions()) {
            askPermissions();
        }


    }

    protected boolean shouldAskPermissions() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    @TargetApi(23)
    protected void askPermissions() {
        String[] permissions = {
                "android.permission.READ_EXTERNAL_STORAGE",
                "android.permission.WRITE_EXTERNAL_STORAGE"
        };
        int requestCode = 200;
        requestPermissions(permissions, requestCode);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission granted and now can proceed
                    notify.displaySnackbar("Permission Granted", 0);

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    notify.displaySnackbar("Permission Denied for Photo Selection... Please allow Ezcrypt to access the pictures to encrypt", 1);
                }
                return;
            }
            // add other cases for more permissions
        }
    }

    public void createFile(String fileName, File oldFile){
        try {

            String filePath = "/storage/emulated/0/DCIM/Camera/";

            File newFile = new File(filePath, fileName);
            copy(oldFile, newFile);

            //sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(newFile)));
            sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://"+newFile.getAbsolutePath())));

            ExifInterface exif = new ExifInterface(newFile.getPath());
            byte[] imageData=exif.getThumbnail();
           // Bitmap  thumbnail= BitmapFactory.decodeByteArray(imageData,0,imageData.length);

           // getContentResolver().update(Uri.fromFile(newFile), null, null, null);
           /// getContentResolver().notifyChange(Uri.fromFile(newFile), null);


        }catch(Exception e){
            notify.displaySnackbar(e.getMessage(), 2);
        }
    }

    public void copy(File src, File dst) throws IOException {
        InputStream in = new FileInputStream(src);
        try {
            OutputStream out = new FileOutputStream(dst);
            try {
                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            } finally {
                out.close();
            }
        } finally {
            in.close();
        }
    }



    static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    public void imageClickedE(View view){
        Helper.setMode("Encrypt");
        imagePreview = (ImageButton) view;
        // 1. on Upload click call ACTION_GET_CONTENT intent
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        // 2. pick image only
        intent.setType("image/*");
        // 3. start activity
        startActivityForResult(intent, 0);

        // define onActivityResult to do something with picked image
    }

    public void imageClickedD(View view){
        Helper.setMode("Decrypt");
        imagePreview = (ImageButton) view;
        // 1. on Upload click call ACTION_GET_CONTENT intent
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        // 2. pick image only
        intent.setType("image/*");
        // 3. start activity
        startActivityForResult(intent, 0);

        // define onActivityResult to do something with picked image
    }

    //Unused moved to decryptController.... will remove soon
    public void decryptButtonClicked(View view){

        File input = new File(inputPath);
        File cover = new File("/storage/emulated/0/DCIM/Camera/","Cover.jpg");
        File secret = new File("/storage/emulated/0/DCIM/Camera/","whatever.txt");
        File stego = new File("/storage/emulated/0/DCIM/Camera/","Stego.jpg");

        try {
            Jstego js = new Jstego(stego);
            System.out.println("Huffman decoding...");
            js.huffmanDecode();
            System.out.println("Seeking information...");
            js.getSecretFileInfo();
            int type = js.getStegoType();
            if (type == 1) {
                js.jstegSeek();
            } else if (type == 2) {
                js.f5Seek();
            } else {
                throw new Exception();
            }
            String algo = null;
            if (type == 1) {
                algo = "Jsteg";
            } else if (type == 2) {
                algo = "F5";
            }
            System.out.println("Seek complete. Algorithm is " + algo);

        }catch(Exception E){
            System.out.println("Error: "+ E.getMessage());
        }
        //encoder.new Encode().execute();

    }
    /*

    //Unused moved to EncryptController.... will remove soon
    public void encryptButtonClicked(View view){


        //Build the notification
        notification.setSmallIcon(R.drawable.ic_lock_black_24dp);
        notification.setTicker("Notification ticker");
        notification.setWhen(System.currentTimeMillis());
        notification.setContentTitle("Notification title");
        notification.setContentText("Notification text");

        Intent intent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        notification.setContentIntent(pendingIntent);

        //Build notification and issues it
        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        nm.notify(uniqueID, notification.build());

        //displaying snackbar
        //TODO: Change displaySnackbar input according to button behavior
        try{
           // inputFile = new File(inputPath);




            //Image image1 = net.windward.android.imageio.ImageIO.read(inputFile);

            //createFile("test2.jpg", inputFile);
            //displaySnackbar("Finished Encoding", 0);

        }catch(Exception e){
            notify.displaySnackbar(e.getMessage(), 2);
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String dateTime = sdf.format(new Date());

        File input = new File(inputPath);
        File cover = new File("/storage/emulated/0/DCIM/Camera/","Cover.jpg");
        File secret = new File(inputPath);
        File stego = new File("/storage/emulated/0/DCIM/Camera/","Stego.jpg");
        Integer stegoType = 1;
        try {
            Jstego js = new Jstego();
            System.out.println("Generating cover image...");
            js.generateCover(input, cover, this.getApplicationContext());
            System.out.println("Huffman decoding...");
            js.huffmanDecode();
            if (stegoType == 1) {
                js.jstegHide(secret);
            } else if (stegoType == 2) {
                js.f5Hide(secret);
            } else {
                throw new Exception();
            }

            System.out.println("Huffman encoding...");
            js.huffmanEncode(stego, this.getApplicationContext());
            System.out.println("Hide complete.");
            notify.displaySnackbar("Finished Encoding", 0);
        }catch(Exception E){
            System.out.println("Error: "+ E.getMessage());
        }

    }*/
    //TODO: Add old school Gallery functionality?
    //Mother fucker "targetSdkVersion is 24 or higher needs to use fileProvider"
    @Override
    protected void onActivityResult(int reqCode, int resCode, Intent data) {
        try{
            if(resCode == Activity.RESULT_OK && data != null) {
                String realPath;
                // SDK < API11
                if (Build.VERSION.SDK_INT < 11)
                        realPath = RealPathUtil.getRealPathFromURI_BelowAPI11(this, data.getData());

                    // SDK >= 11 && SDK < 19
                else if (Build.VERSION.SDK_INT < 19)
                    realPath = RealPathUtil.getRealPathFromURI_API11to18(this, data.getData());

                    // SDK > 19 (Android 4.4)
                else
                    realPath = RealPathUtil.getRealPathFromURI_API19(this, data.getData());
                inputPath = realPath;
                if(Helper.getMode().equals("Encrypt")){
                    helper.setPathE(inputPath);
                    helper.setPathD("");
                }else{
                    helper.setPathD(inputPath);
                    helper.setPathE("");
                }

                notify.displaySnackbar("Photo Selected" + realPath, 0);
                displayPreviewFromPath(realPath);
            }
        }catch (Exception e){
            notify.displaySnackbar(e.getMessage(), 2);
        }


    }

    public void displayPreviewFromPath(String realPath){
        Uri uriFromPath = Uri.fromFile(new File(realPath));

        // you have two ways to display selected image

        // ( 1 ) imageView.setImageURI(uriFromPath);

        // ( 2 ) imageView.setImageBitmap(bitmap);
        Bitmap bitmap = null;
        try {
            bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(uriFromPath));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        imagePreview.setImageBitmap(bitmap);
    }



    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends android.support.v4.app.FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    EncryptController tab1 = new EncryptController();
                    return tab1;
                case 1:
                    DecryptController tab2 = new DecryptController();
                    return tab2;
                case 2:
                    About tab3 = new About();
                    return tab3;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "ENCRYPT";
                case 1:
                    return "DECRYPT";
                case 2:
                    return "ABOUT";
            }
            return null;
        }
    }
}
