package io.ezcrypt.ezcrypt;

import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

/**
 * Created by suppakng on 7/23/2017.
 */

public class Notification {
    private static Notification instance;
    private static Snackbar sb;
    private Helper helper = Helper.get();

    private Notification(){

    }

    public static Notification get() {
        if (instance == null) instance = getSync();
        return instance;
    }

    private static synchronized Notification getSync() {
        if (instance == null) instance = new Notification();
        return instance;
    }

    public void setSnackBar(View view, String errorMessage, Integer time){
        sb = Snackbar.make(view, errorMessage, time);
    }

    public void displaySnackbar(String errorMessage, int type){

        View sbView = sb.getView();
        //Snackbar
        //0 : Warning
        //1 : Error
        //2  : Success
        sb.setText(errorMessage);

        if (type == 1){
            sbView.setBackgroundColor(helper.getResource().getColor(R.color.warning));
        } else if (type == 2) {
            sbView.setBackgroundColor(helper.getResource().getColor(R.color.error));
        } else {
            sbView.setBackgroundColor(helper.getResource().getColor(R.color.success));
        }
        sb.show();
    }

}
