package io.ezcrypt.ezcrypt;

/**
 * Created by Simon on 2017-06-13.
 */

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.File;

import io.ezcrypt.ezcrypt.encoder.Jstego;
import io.ezcrypt.ezcrypt.encryptor.Encryptor;

/**
 * Created by Simon on 2017-06-05.
 */

public class DecryptController extends Fragment{
    Notification notification = Notification.get();
    Helper helper = Helper.get();
    private Encryptor encryptor = Encryptor.getInstance();
    String errors = "";
    EditText passwordD;
    TextView messageD;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.decrypt, container, false);
        passwordD = (EditText) rootView.findViewById(R.id.passwordD);
        messageD = (TextView) rootView.findViewById(R.id.messageDecrypted);
        Button decryptB = (Button) rootView.findViewById(R.id.decryptButton);
        decryptB.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                decrypt();
            }
        });

        //your shit goes here
        return rootView;
    }

    private void decrypt(){
        if(isValid()) {
            //Decode
            byte[] m3 = decode();

            //Decrypt
            String m1 = encryptor.decrypt(passwordD.getText().toString(), m3);

            if(m1 == null || m1.equals("")){
                notification.displaySnackbar("Invalid Password or Picture", 2);
            }
            messageD.setText(m1);
            notification.displaySnackbar("Image Decrypted", 0);

        }else{
            notification.displaySnackbar(errors, 1);
        }

    }
    public static void printBytesArray(byte[] array){

        String b = "";
        for(int i = 0; i < array.length; i++){
            b += array[i];
        }

        System.out.println("printArray: "+b);
    }

    public boolean isValid(){
        errors = "Please enter the ";

        if(passwordD.getText().toString().trim().equals("")){
            errors += "password, ";
        }

        if(helper.getPathD() == null || helper.getPathD().equals("")){
            errors += "image";
        }

        if(errors.contains("password")||errors.contains("image")){
            return false;
        }
        return true;
    }

    private byte[] decode(){

        File stego = new File(helper.getPathD());
        byte[] m3 = null;

        try {
            Jstego js = new Jstego(stego);
            System.out.println("Huffman decoding...");
            js.huffmanDecode();
            System.out.println("Seeking information...");
            js.getSecretFileInfo();
            int type = js.getStegoType();
            if (type == 1) {
                m3 = js.jstegSeek();
            } else if (type == 2) {
                js.f5Seek();
            } else {
                throw new Exception();
            }
            String algo = null;
            if (type == 1) {
                algo = "Jsteg";
            } else if (type == 2) {
                algo = "F5";
            }
            System.out.println("Seek complete. Algorithm is " + algo);
            return m3;
        }catch(Exception E){
            System.out.println("Error: "+ E.getMessage());
            notification.displaySnackbar("Internal Error Decoder", 2);
        }

        return null;

    }

}
