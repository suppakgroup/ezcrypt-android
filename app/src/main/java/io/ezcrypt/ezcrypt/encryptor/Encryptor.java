package io.ezcrypt.ezcrypt.encryptor;

import android.util.Base64;
import android.util.Log;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.nio.charset.StandardCharsets;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SealedObject;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import android.app.Activity;
import android.os.AsyncTask;

import io.ezcrypt.ezcrypt.Helper;


/**
 * Created by ryant_000 on 7/10/2017.
 */

public class Encryptor {


    //sixteen byte string for the IVspec
    private static final String IV = "AAAAAAAAAAAAAAAA";
    private static final String Salt = "IfYouLookForTheLightYouCanOftenFindIt_Ryan_ButIfYouLookForTheDark,ThatIsAllYoullEverSee_Nuthapol";
    //standard AES recommended iterations
    //private static final int ITERATIONS = 65536;
    //TODO: Change key length to 256
    private static final int KEY_LENGTH = 128;

    private Helper helper = Helper.get();

    private static Encryptor instance = null;
    private static Activity activity = null;

    public Encryptor(){
        // Exists only to defeat instantiation.
    }

    /**
     * Take the first 32 bytes of a byte array value
     * @param hashedPassword
     * @return byte[]
     */
    public static byte[] byteTo32Byte(byte[] hashedPassword) {

        byte[] temp = new byte[32];

          for (int i=0; i <31; i++){
                if(i >= hashedPassword.length){
                    temp[i] = 0;
                    continue;
                } else{
                    temp[i] = hashedPassword[i];
                }
            }
        return temp;
    }

    public void setContext(Activity a) {activity = a;}

    public static Encryptor getInstance(){
        if(instance == null){
            instance = new Encryptor();
        }
        return instance;
    }
    
    public class Encrypt extends AsyncTask<Integer, Void, String>{
        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            //mProgress.setProgress(0);
            //mProgress.setVisibility(View.Visible);
            //Button encryptB = (Button) activity.findViewById(R.id.encodeB);
            //encryptB.setText("Encoding...");
        }

        @Override
        protected void onProgressUpdate(Void... values){

        }

        @Override
        protected String doInBackground(Integer... params){
           // Log.d(TAG, "Started Process");
            float test = 0;
            try{
                //Do stuff
             //   Log.d(TAG,"Do Stuff!");
            }catch(Exception e){

            }

           // Log.d(TAG,"Finished process");
            return "Done";
        }

        @Override
        protected void onPostExecute(String result){
            // Button encryptB = (Button) activity.findViewById(R.id.encrypt/decryptB);
            //encryptB.setText("Finished");
        }

    }

    /**
     * Values used for the main Encryptor class
     */

    public String decrypt(String p1, byte[] message){
        try {
            System.out.println("printing encryptedM3");
            printBytesArray(message);
            String hashedPassword = hashSHA256(p1);
            byte[] decryptedMessageRSA = decryptRSA(helper.getKeyPair().getPrivate(), message);

            System.out.println("dRSA: " + decryptedMessageRSA[0] + " " + decryptedMessageRSA[1]);
            printBytesArray(decryptedMessageRSA);

            String m3 = decryptAES(decryptedMessageRSA, hashedPassword);

            System.out.println("m3: " + m3);

            return m3;
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return null;
    }

    public static void printBytesArray(byte[] array){

        String b = "";
        for(int i = 0; i < array.length; i++){
            b += array[i];
        }

        System.out.println("printArray: "+b);
    }

    public  byte[] encrypt(String p1, String message) {
        try{
            String hashedPassword = hashSHA256(p1);

            byte[] m2 = encryptAES(message.toString(), hashedPassword);
            byte[] encryptedMessageRSA = encryptRSA(helper.getKeyPair().getPublic(), m2);

            return  encryptedMessageRSA;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return  null;
    }

    public static PublicKey getKey(String key){
        try{
            byte[] byteKey = Base64.decode(key.getBytes(), Base64.DEFAULT);
            X509EncodedKeySpec X509publicKey = new X509EncodedKeySpec(byteKey);
            KeyFactory kf = KeyFactory.getInstance("RSA");

            return kf.generatePublic(X509publicKey);
        }
        catch(Exception e){
            e.printStackTrace();
        }

        return null;
    }

    private static byte[] convertToBytes(Object object) throws IOException {
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
             ObjectOutput out = new ObjectOutputStream(bos)) {
            out.writeObject(object);
            return bos.toByteArray();
        }
    }

    private static Object convertFromBytes(byte[] bytes) throws IOException, ClassNotFoundException {
        try (ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
             ObjectInput in = new ObjectInputStream(bis)) {
            return in.readObject();
        }
    }

    public static byte[] encryptData(byte[] data, PublicKey publicKey) {
        try {
            Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPWITHSHA-256ANDMGF1PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);
            byte[] encryptedData = cipher.doFinal(data);
            return encryptedData;
        } catch (Exception d) {
            System.out.println(d.getMessage());
            return null;
        }

    }
    public static String decryptData(byte[] data, PublicKey privateKey) {
        try {

            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
            return new String(cipher.doFinal(data),"utf-8");
        } catch (Exception e) {
            return null;
        }
    }

    public static byte[] encryptRSA(PublicKey publicKey, byte[] m2) {

        SealedObject myEncryptedMessage = null;
        byte[] encryptedMessage = null;
        try {
            Cipher c = Cipher.getInstance("RSA");
            c.init(Cipher.ENCRYPT_MODE, publicKey);
            encryptedMessage = c.doFinal(m2);
        } catch (Exception e)
        {
            System.out.println(e.toString());
        }
        return encryptedMessage;
    }

    public static byte[] decryptRSA(PrivateKey privateKey, byte[] m2) {

        SealedObject sealedObj = null;
        byte[] decryptedMessage = null;

        try {
            Cipher c = Cipher.getInstance("RSA");
            c.init(Cipher.DECRYPT_MODE, privateKey);
            decryptedMessage = c.doFinal(m2);
        } catch (Exception e) {
            System.out.println(e.toString());
        }

        return decryptedMessage;
    }

    public static byte[] encryptAES(String plainText, String hashedPassword) throws Exception {


        byte[] encodedKey     = byteTo32Byte( Base64.decode(hashedPassword, Base64.DEFAULT));
        SecretKey sKey = new SecretKeySpec(encodedKey, 0, encodedKey.length, "AES");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, sKey, new IvParameterSpec(IV.getBytes("UTF-8")));
        return cipher.doFinal(plainText.getBytes("UTF-8"));
    }

    public static String decryptAES(byte[] encryptedText, String hashedPassword) throws Exception {
        try {
            byte[] encodedKey = byteTo32Byte(Base64.decode(hashedPassword, Base64.DEFAULT));
            SecretKey sKey = new SecretKeySpec(encodedKey, 0, encodedKey.length, "AES");


            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, sKey, new IvParameterSpec(IV.getBytes("UTF-8")));
            return new String(cipher.doFinal(encryptedText), "UTF-8");
        }catch(Exception e){
            System.out.println(e.getMessage());
        }

        return "";
    }

    public static SecretKey generateSecretKey() throws NoSuchAlgorithmException {
        // Generate a 256-bit key
        final int outputKeyLength = 256;

        SecureRandom secureRandom = new SecureRandom();

        // Do *not* seed secureRandom! Automatically seeded from system entropy.
        KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
        keyGenerator.init(outputKeyLength, secureRandom);
        SecretKey key = keyGenerator.generateKey();
        return key;
    }

    public static String hashSHA256(String message) {
        String TAG = "Error";
        byte[] hash = null;
        StringBuffer hexString = new StringBuffer();
        Log.d(TAG, " == Before SHA 256 Hashing == " + " : " + message);

        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            digest.update(Salt.getBytes());
            hash = digest.digest(message.getBytes(StandardCharsets.UTF_8));

        } catch (Exception e) {
            Log.d(TAG, e.toString());
        }

        for (int i = 0; i < hash.length; i++) {
            if ((0xff & hash[i]) < 0x10) {
                hexString.append("0"
                        + Integer.toHexString((0xFF & hash[i])));

            } else {
                hexString.append(Integer.toHexString(0xff & hash[i]));
            }

        }
        Log.d(TAG, " == After SHA 256 Hashing == " + " : " + hexString);
        return hexString.toString();
    }


}
