package io.ezcrypt.ezcrypt;

/**
 * Created by Simon on 2017-06-13.
 */

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import io.ezcrypt.ezcrypt.encoder.Jstego;
import io.ezcrypt.ezcrypt.encryptor.Encryptor;

import static android.content.Context.NOTIFICATION_SERVICE;

public class EncryptController extends Fragment {
    View rootView;
    Helper helper = Helper.get();
    private Encryptor encryptor = Encryptor.getInstance();
    Notification notification = Notification.get();
    EditText passwordE;
    EditText messageE;
    String errors = "";

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.encrypt, container, false);

        messageE = (EditText) rootView.findViewById(R.id.messageE);
        passwordE = (EditText) rootView.findViewById(R.id.passwordE);
        Button encryptB = (Button) rootView.findViewById(R.id.encryptButton);
        encryptB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                encrypt();
            }
        });
        return rootView;
    }

    public static void printBytesArray(byte[] array) {

        String b = "";
        for (int i = 0; i < array.length; i++) {
            b += array[i];
        }

        System.out.println("printArray: " + b);
    }
    public void initialCompress() throws Exception {
        String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
        InputStream in = new FileInputStream(Helper.getPathE());
        Bitmap bm2 = BitmapFactory.decodeStream(in);
        OutputStream stream = new FileOutputStream("/storage/emulated/0/DCIM/Camera/Temp1.JPG");
        bm2.compress(Bitmap.CompressFormat.JPEG, 20, stream);
        rootView.getContext().getApplicationContext().sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("/storage/emulated/0/DCIM/Camera/Temp1.JPG")));
        stream.close();
        in.close();

        Helper.getContext().sendBroadcast(new Intent(
                Intent.ACTION_MEDIA_MOUNTED,
                Uri.parse("file://" + Environment.getExternalStorageDirectory())));

    }
    public void encrypt() {
        /*
        try {
            initialCompress();
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        //Encrypt
        if(isValid()){
            byte[] encryptedMessage = encryptor.encrypt(passwordE.getText().toString(), messageE.getText().toString());
            //Encode
            encode(encryptedMessage);
            notification.displaySnackbar("Finished Encryption", 0);
        }else{
            notification.displaySnackbar(errors, 1);
        }

        //System.out.println("MessageI: " + encryptedMessage);
        //printBytesArray(encryptedMessage);
        //Decrypt
        //String decryptMessage = encryptor.decrypt("MyPassword",encryptedMessage);
        //System.out.println("dm: "+decryptMessage);

    }

    public boolean isValid(){
        errors = "Please enter the ";

        if(passwordE.getText().toString().trim().equals("")){
            errors += "password, ";
        }

        if(messageE.getText().toString().trim().equals("")){
            errors += "message, ";
        }

        if(helper.getPathE() == null || helper.getPathE().equals("")){
            errors += "image";
        }

        if(errors.contains("password")||errors.contains("message")||errors.contains("image")){
            return false;
        }
        return true;
    }

    public void encode(byte[] encryptedMessage) {
        try {
            String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());

            File input = new File(helper.getPathE());
            File cover = new File("/storage/emulated/0/DCIM/Camera/", "Encrypt-"+date+".jpg");
            File secret = new File(helper.getPathE());
            File stego = new File("/storage/emulated/0/DCIM/Camera/", "Stego.jpg");
            Integer stegoType = 1;

            Jstego js = new Jstego();
            System.out.println("Generating cover image...");
            js.generateCover(input, cover, rootView.getContext().getApplicationContext());
            System.out.println("Huffman decoding...");
            js.huffmanDecode();
            if (stegoType == 1) {
                js.jstegHide(secret, encryptedMessage);
            } else if (stegoType == 2) {
                js.f5Hide(secret);
            } else {
                throw new Exception();
            }

            System.out.println("Huffman encoding...");
            js.huffmanEncode(cover, rootView.getContext().getApplicationContext());
            System.out.println("Hide complete.");
            //displaySnackbar("Finished Encoding", 0);
        } catch (Exception E) {
            System.out.println("Error: " + E.getMessage());
            notification.displaySnackbar("Internal Error Encoder", 2);
        }
    }


}
